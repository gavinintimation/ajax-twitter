<?php

if ( !function_exists('gmtw_add_scripts') ) {

	function gmtw_add_scripts() {

		wp_enqueue_style( 'gmtw-main-style', plugins_url() . '/ajax-twitter/css/style.css' );
		wp_enqueue_script( 'gmtw-main-script', plugins_url() . '/ajax-twitter/js/main.js' );

		$data = array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) );
		wp_localize_script( 'gmtw-main-script', 'gmtwAjaxUrl', $data );

	}

	add_action( 'wp_enqueue_scripts', 'gmtw_add_scripts' );

}

if ( !function_exists('gmtw_add_admin_scripts') ) {

	function gmtw_add_admin_scripts() {

		wp_enqueue_style( 'gmtw-admin-style', plugins_url() . '/ajax-twitter/css/admin.css' );

	}

	add_action( 'admin_enqueue_scripts', 'gmtw_add_admin_scripts' );

}