<?php

class Ajax_Twitter_Widget extends WP_Widget {

	public function __construct() {

		$widget_ops = array( 
			'classname' => 'gm_twitter_widget',
			'description' => 'Fetch tweets via AJAX',
		);

		parent::__construct( 'gm_twitter_widget', 'Ajax Twitter Widget', $widget_ops );

	}

	public function widget( $args, $instance ) {
		
		echo $args['before_widget'];

		echo '<div id="' . $args['widget_id']  . '" class="gmtw-twitter-widget">';

		if ( !empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		echo '</div>';

		echo $args['after_widget'];

		$ajaxData = array( 'target' => $args['widget_id'], 'tweetCount' => $instance['tweetCount'], 'username' => $instance['username'] );

		echo '<script>
		window.addEventListener("DOMContentLoaded", function() { 
			(function($) {
				$(document).ready( function() {
					get_tweets(' . json_encode($ajaxData) . ');
				});
			})(jQuery);
		});
		</script>';

	}

	public function form( $instance ) {

		$title = !empty( $instance['title'] ) ? $instance['title'] : 'New title';
		$username = !empty( $instance['username'] ) ? $instance['username'] : '';
		$tweetCount = !empty( $instance['tweetCount'] ) ? $instance['tweetCount'] : '';

		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_attr_e( 'Title:' ) ?>
			</label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>">
				<?php esc_attr_e( 'Username:' ) ?>
			</label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" value="<?php echo esc_attr( $username ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'tweetCount' ) ); ?>">
				<?php esc_attr_e( 'Number of tweets:' ) ?>
			</label>
			<input type="number" id="<?php echo esc_attr( $this->get_field_id( 'tweetCount' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tweetCount' ) ); ?>" value="<?php echo esc_attr( $tweetCount ); ?>">
		</p>

		<?php
		
	}

	public function update( $new_instance, $old_instance ) {

		$instance = array();

		$instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['username'] = ( !empty( $new_instance['username'] ) ) ? strip_tags( $new_instance['username'] ) : '';
		$instance['tweetCount'] = ( !empty( $new_instance['tweetCount'] ) ) ? strip_tags( $new_instance['tweetCount'] ) : '';

		return $instance;
		
	}

}