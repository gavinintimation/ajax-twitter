<?php

function gmtw_settings_page() {
	?>
	<div id="gmtw_settings_page" class="wrap">
		<form method="post" action="options.php">
			<?php
			settings_fields( 'gmtw_twitter_settings' );
			do_settings_sections( 'gmtw_settings_page' );      
			submit_button(); 
			?>          
		</form>
	</div>
	<?php
}

function add_gmtw_settings_page() {
	add_menu_page( 'Twitter Settings', 'Twitter Settings', 'manage_options', 'gmtw-twitter-options', 'gmtw_settings_page', null, 99);
}

add_action( 'admin_menu', 'add_gmtw_settings_page' );

function gmtw_display_settings_fields() {

	add_settings_section( 'gmtw_twitter_settings', 'Twitter Settings', null, 'gmtw_settings_page' );
	
	add_settings_field( 'gmtw_access_token', 'Access Token', 'gmtw_display_access_token', 'gmtw_settings_page', 'gmtw_twitter_settings' );

	add_settings_field( 'gmtw_access_token_secret', 'Access Token Secret', 'gmtw_display_access_token_secret', 'gmtw_settings_page', 'gmtw_twitter_settings' );

	add_settings_field( 'gmtw_consumer_key', 'Consumer Key', 'gmtw_display_consumer_key', 'gmtw_settings_page', 'gmtw_twitter_settings' );

	add_settings_field( 'gmtw_consumer_secret', 'Consumer Secret', 'gmtw_display_consumer_secret', 'gmtw_settings_page', 'gmtw_twitter_settings' );

	register_setting( 'gmtw_twitter_settings', 'gmtw_access_token' );
	register_setting( 'gmtw_twitter_settings', 'gmtw_access_token_secret' );
	register_setting( 'gmtw_twitter_settings', 'gmtw_consumer_key' );
	register_setting( 'gmtw_twitter_settings', 'gmtw_consumer_secret' );

}

add_action( 'admin_init', 'gmtw_display_settings_fields');


function gmtw_display_access_token() {
	?>
	<input type="text" name="gmtw_access_token" id="gmtw_access_token" value="<?php echo get_option('gmtw_access_token'); ?>" />
	<?php
}

function gmtw_display_access_token_secret() {
	?>
	<input type="text" name="gmtw_access_token_secret" id="gmtw_access_token_secret" value="<?php echo get_option('gmtw_access_token_secret'); ?>" />
	<?php
}

function gmtw_display_consumer_key() {
	?>
	<input type="text" name="gmtw_consumer_key" id="gmtw_consumer_key" value="<?php echo get_option('gmtw_consumer_key'); ?>" />
	<?php
}

function gmtw_display_consumer_secret() {
	?>
	<input type="text" name="gmtw_consumer_secret" id="gmtw_consumer_secret" value="<?php echo get_option('gmtw_consumer_secret'); ?>" />
	<?php
}