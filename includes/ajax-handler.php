<?php

if ( !function_exists( 'gmtw_get_tweets' ) ) {

	add_action( 'wp_ajax_nopriv_gmtw_get_tweets', 'gmtw_get_tweets' );
	add_action( 'wp_ajax_gmtw_get_tweets', 'gmtw_get_tweets' );

	function gmtw_get_tweets() {

		require_once( plugin_dir_path(__FILE__) . '/class.twitterapiexchange.php' );

		$username = !empty( $_POST['data']['username'] ) ? filter_var( $_POST['data']['username'], FILTER_SANITIZE_STRING ) : '@increatives';

		if ( !empty( $_POST['data']['tweetCount'] ) && intval( $_POST['data']['tweetCount'] ) > 0 ) {

			$tweetCount = ( intval( $_POST['data']['tweetCount'] ) <= 10 ) ? filter_var( $_POST['data']['tweetCount'], FILTER_SANITIZE_NUMBER_INT ) : 10;

		} else {

			$tweetCount = 1;
			
		}

		$settings = array(
			'oauth_access_token'          => get_option('gmtw_access_token'),
			'oauth_access_token_secret'   => get_option('gmtw_access_token_secret'),
			'consumer_key'                => get_option('gmtw_consumer_key'),
			'consumer_secret'             => get_option('gmtw_consumer_secret')
		);

		$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
		$getfield = '?screen_name=' . $username . '&count=' . $tweetCount;
		$requestMethod = 'GET';

		$twitter = new TwitterAPIExchange($settings);
		$response = json_decode( $twitter
			->setGetfield($getfield)
			->buildOauth($url, $requestMethod)
			->performRequest()
		);

		$tweets = array();

		foreach ( $response as $tweet ) {
			$tempTime = DateTime::createFromFormat('D M j H:i:s P Y', $tweet->created_at);
			$a['time'] = $tempTime->format('F j, Y g:i a');
			/* links */
			$a['text'] = $tweet->text;
			$a['text'] = preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@', '<a href="$1">$1</a>', $a['text']);
			/* users */
			$a['text'] = preg_replace('/@(\w+)/', '<a href="http://twitter.com/$1">@$1</a>', $a['text']);
			/* hashtags */
			$a['text'] = preg_replace('/\s+#(\w+)/', ' <a href="https://twitter.com/hashtag/$1">#$1</a>', $a['text']);
			array_push($tweets, $a);
		}

		wp_send_json_success($tweets);

	}

}