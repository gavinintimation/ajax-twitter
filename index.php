<?php

/* 
Plugin Name: Ajax Twitter
Description: Fetch tweets via AJAX
Version: 1.0.0
Author: Gavin McDonald
*/

/* Exit if accessed directly */
if ( !defined('ABSPATH') ) {
	exit();
}

require_once( plugin_dir_path(__FILE__) . '/includes/ajax-handler.php' );
require_once( plugin_dir_path(__FILE__) . '/includes/scripts.php' );
require_once( plugin_dir_path(__FILE__) . '/includes/ajax-twitter-class.php' );
require_once( plugin_dir_path(__FILE__) . '/includes/settings.php' );

/* Register the widget */

if ( !function_exists( 'register_ajax_twitter' ) ) {

	function register_ajax_twitter() {

		register_widget( 'Ajax_Twitter_Widget' );

	}

}

add_action( 'widgets_init', 'register_ajax_twitter' );