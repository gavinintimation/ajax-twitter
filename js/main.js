function get_tweets(gmtwAjaxData) {

	var target = '#' + gmtwAjaxData.target;

	jQuery.ajax({

		url: gmtwAjaxUrl['ajaxurl'],
		type: 'post',
		dataType: 'json',
		data: {
			action: 'gmtw_get_tweets',
			data: gmtwAjaxData
		},
		success: function(r) {

			r.data.forEach( function(tweet) {

				var tweetElement = jQuery('<div class="tweet"></div>');
				tweetElement.append('<p>' + tweet.text + '</p>');
				tweetElement.append('<p class="time">' + tweet.time + '</p>');
				jQuery(target).append(tweetElement);

			} );

		}

	});

}